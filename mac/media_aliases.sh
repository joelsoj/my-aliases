#### Youtube DL
youtube-dl_audio-best() {
    youtube-dl --extract-audio --audio-format best --audio-quality 0 $1
}
youtube-dl_audio-mp3() {
    youtube-dl --extract-audio --audio-format mp3 --audio-quality 0 --embed-thumbnail $1
}
youtube-dl_audio-aac() {
    youtube-dl --extract-audio --audio-format aac --audio-quality 0 $1
}
youtube-dl_audio-flac() {
    youtube-dl --extract-audio --audio-format flac --audio-quality 0 $1
}