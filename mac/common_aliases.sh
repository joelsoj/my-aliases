### System
alias ll='ls -salhF'
alias history='HISTTIMEFORMAT="%d/%m/%y %T - " && history'

alias disp_groups='dscl . list /groups'
alias disp_users='dscl . -list /Users'

alias edit_zshrc='subl ~/.zshrc'
alias edit_zprofile='subl ~/.zprofile'
alias edit_ssh_config='subl ~/.ssh/config'

sizeof() {
    target=$1
    depth="${2:-0}"
    du -h -d $depth $target
}

abs_path_copy() {
    echo $(pwd)/$1 | pbcopy
}

abs_path_print() {
    echo $(pwd)/$1
}

### Monitoring
alias topmem='ps aux -m | head'
alias topcpu='ps aux -r | head'


# Network
alias dispproxy='env | grep -i proxy'
alias disp_ports='sudo lsof -P -i'
alias disp_ports_listen='sudo lsof -P -i | grep LISTEN'

disp_port() {
    port=$1
    sudo lsof -P -i :$port
}

ipinfo() {
    curl ipinfo.io
    echo 
}

### Apps
alias subl='"/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"'
ideal() {
    idea -e "$@"
}

### Dock
alias dock_add_space="defaults write com.apple.dock persistent-apps -array-add '{tile-type="spacer-tile";}' && killall Dock"