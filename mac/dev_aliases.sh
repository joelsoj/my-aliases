### Git
alias git_commit_types='cat "$GOOGLE_DRIVE_HOME"/Programming*/Git/commit_types.txt'
alias git_rebase_master='git fetch origin && git rebase origin/master && git push -f && git checkout master && git pull origin master'

### SDKMAN
alias sdk_list_java_installed='sdk list java | grep installed'

### Localstack
alias awslocal="aws --endpoint-url=http://localhost:4566"
