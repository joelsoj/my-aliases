declare -a coreModules=(
  common
)

declare -a optionalModules=(
  dev
  navigation
  media
  media-server
  docker
  work
)

declare -a allModules=(
  "${coreModules[@]}"
  "${optionalModules[@]}"
)

declare -A moduleDownloadLinks=(
  [common]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/common_aliases.sh"
  [dev]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/dev_aliases.sh"
  [navigation]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/navigation_aliases.sh"
  [media]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/media_aliases.sh"
  [media-server]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/media_server.sh"
  [docker]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/docker_aliases.sh"
  [work]="https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/work_aliases.sh"
)

# Update the core script
#
my_aliases_update() {
  curl --create-dirs -L -O --output-dir "$HOME/.my_aliases/" "https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/my_aliases_core.sh"
  echo "Update downloaded."
  echo "=> Run 'refresh_zshrc' or reset terminal session to apply update."
}

# Clean up the  ~/.my_aliases folder removing all the pulled aliases.
#
my_aliases_uninstall() {
  rm -r "$HOME/.my_aliases"
  echo "My Aliases has been removed."
  echo "Dont''t forget to clean your zshrc"
  echo "=> Reset terminal session to apply changes."
}

# Display all available modules
#
my_aliases_list_modules() {
  for module in "${coreModules[@]}"; do
    downloadLink=${moduleDownloadLinks[$module]}
    echo "Core '$module' - URL: $downloadLink"
  done
  for module in "${optionalModules[@]}"; do
    downloadLink=${moduleDownloadLinks[$module]}
    echo "Optional '$module' - URL: $downloadLink"
  done
}

# Pull aliases from git master branch and store it under "~/.my_aliases" folder.
# Use this to fetch remote changes and stay up to date.
# This will overwrite current aliases!
#
my_aliases_pull() {
  for module in "${allModules[@]}"; do
    downloadLink=${moduleDownloadLinks[$module]}
    echo "Downloading module $module - URL: $downloadLink"
    curl "$downloadLink" -L -o "$HOME/.my_aliases/${module}_aliases.sh" --create-dirs
  done
  my_aliases_load
  echo "Aliases updated successfully."
}

# Load the aliases from each module if it has been pulled.
#
my_aliases_load() {
  for module in "${allModules[@]}"; do
    __my_aliases_source "$module"
  done
}

# Source aliases for module passed as argument.
#
# $1 - Module for which to load aliases
#
__my_aliases_source() {
  module=$1
  aliasesFile="$HOME/.my_aliases/${module}_aliases.sh"
  if [ -f "$aliasesFile" ]; then
    # echo "Sourcing $aliasesFile" # uncomment for debug
    # shellcheck source="~/.my_aliases/${module}_aliases.sh"
    source "$aliasesFile"
  fi
}

###################################################
# Load the aliases each time this script is sourced
###################################################
my_aliases_load
