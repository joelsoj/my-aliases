# Plex Server
alias goto_plex='cd /Users/jjoelson/Library/"Application Support"/"Plex Media Server"'
alias goto_plex_logs='cd /Users/jjoelson/Library/Logs/"Plex Media Server"'
alias watch_plex='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"Plex Media Server.log"'
alias watch_plex_analysis='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"Plex Media Scanner Analysis.log"'
alias watch_plex_analysis_deep='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"Plex Media Scanner Deep Analysis.log"'
alias watch_plex_scanner='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"Plex Media Scanner.log"'
alias watch_plex_transcode='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"Plex Transcoder Statistics.log"'
alias watch_plex_plugin_localmedia='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"PMS Plugin Logs"/com.plexapp.agents.localmedia.log'
alias watch_plex_plugin_tmdb='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"PMS Plugin Logs"/com.plexapp.agents.themoviedb.log'
alias watch_plex_plugin_tvdb='tail -f /Users/jjoelson/Library/Logs/"Plex Media Server"/"PMS Plugin Logs"/com.plexapp.agents.thetvdb.log'
alias pms='/Applications/"Plex Media Server.app"/Contents/MacOS/"Plex Media Scanner"'

# Sonarr
alias log_sonarr='tail -f ~/.config/NzbDrone/logs/sonarr.txt'

# Radarr
alias log_radarr='tail -f ~/.config/Radarr/logs/radarr.txt'

# Jackett
alias start_jackett='launchctl load ~/Library/LaunchAgents/org.user.Jackett.plist'
alias stop_jackett='launchctl unload ~/Library/LaunchAgents/org.user.Jackett.plist'
alias log_jackett='tail -f ~/.config/Jackett/log.txt'

# qBittorrent

# FlareSolverr
alias log_flare='docker compose -p flaresolverr logs -f'