# My Aliases

## Installation

**Only zsh is supported for the moment.**

1. Run the following command.

```
curl --create-dirs -L -O --output-dir ~/.my_aliases/ "https://gitlab.com/joelsoj/my-aliases/-/raw/master/mac/my_aliases_core.sh"
```

2. Add the following block to your zshrc file.

```
#===============
# My Aliases
#===============
alias refresh_zshrc="source $HOME/.zshrc"
[[ -s "$HOME/.my_aliases/my_aliases_core.sh" ]] && source "$HOME/.my_aliases/my_aliases_core.sh"
```

3. Run the following command.

```
source ~/.zshrc
```

## Usage

### Available modules

See available modules by running:

```
my_aliases_list_modules
```

### Pull

Pull the aliases by running:

```
my_aliases_pull
```

Use this to fetch remote changes and stay up to date with the aliases. Pulled aliases will be stored under "~/.my_aliases" folder.  
Do not manual add or edit files in this folder as they will be overwritten by the `pull_aliases` command :warning:  
You can exclude optional modules with the -e option (e.g., `pull_aliases -e docker media`).  
The -e option takes a list of space seperated modules.

## Update

Update the core script by running:

```
my_aliases_update
```

Use this when a new version of the script is available.

## Uninstall

1. Uninstall by running:

```
my_aliases_uninstall
```

2. Clean your zshrc file by removing the following block:

```
#===============
# My Aliases
#===============
alias refresh_zshrc="source $HOME/.zshrc"
[[ -s "$HOME/.my_aliases/my_aliases_core.sh" ]] && source "$HOME/.my_aliases/my_aliases_core.sh"
```

## Roadmap

- [ ] Add -e option to `pull_aliases` command to exclude optional modules